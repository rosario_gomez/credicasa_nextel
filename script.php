<?php

    // Specify API URL
    define('HASOFFERS_API_URL', 'https://api.hasoffers.com/Apiv3/json');
 
    // Specify method arguments
    $args = array(
        'NetworkId' => 'cptaffiliation',
        'Target' => 'Affiliate_Report',
        'Method' => 'getConversions',
        'api_key' => 'e5f48a88ce08d40ec2b4f824165f8ae6729a2baa35a5473e94031d154d5e1e95',
        'fields' => array(
            'Stat.approved_payout',
            'Offer.name',
            'Stat.affiliate_info1',
            'Stat.date',
            'Stat.id',
            'Stat.offer_id',
            'Stat.ad_id'
        ),
        'filters' => array(
            'Stat.date' => array(
                'conditional' => 'BETWEEN',
                'values' => array(
                    '2017-01-22',
                    date( "Y-m-d" )
                )
            )
        ),
        'limit' => '0'
    );
 
    // Initialize cURL
    $curlHandle = curl_init();
 
    // Configure cURL request
    curl_setopt($curlHandle, CURLOPT_URL, HASOFFERS_API_URL . '?' . http_build_query($args));
 
    // Make sure we can access the response when we execute the call
    curl_setopt($curlHandle, CURLOPT_RETURNTRANSFER, true);
 
    // Execute the API call
    $jsonEncodedApiResponse = curl_exec($curlHandle);
 
    // Ensure HTTP call was successful
    if($jsonEncodedApiResponse === false) {
        throw new \RuntimeException(
            'API call failed with cURL error: ' . curl_error($curlHandle)
        );
    }
 
    // Clean up the resource now that we're done with cURL
    curl_close($curlHandle);
 
    // Decode the response from a JSON string to a PHP associative array
    $apiResponse = json_decode($jsonEncodedApiResponse, true);
 
    // Make sure we got back a well-formed JSON string and that there were no
    // errors when decoding it
    $jsonErrorCode = json_last_error();
    if($jsonErrorCode !== JSON_ERROR_NONE) {
        throw new \RuntimeException(
            'API response not well-formed (json error code: ' . $jsonErrorCode . ')'
        );
    }
 
    // Print out the response details
    if($apiResponse['response']['status'] === 1) {
        // No errors encountered
        /*echo 'API call successful';
        echo PHP_EOL;
        echo 'Response Data: ' . print_r($apiResponse['response']['data'], true);
        echo PHP_EOL;*/
        
        //var_dump($apiResponse);
        
        /* Funcion para formular postback */
        
        function call_postback() {
        	global $campaignid; 
        	global $refid;
        	global $date;
        	global $orderid;
        	
        	return $postback = "http://track.clickwise.net/pb?ActionCode=lead&CampaignID=$campaignid&RefId=$refid&OrderId=$orderid&TotalCost=0&Date=$date";

        }
        
        
        $j = count($apiResponse["response"]["data"]["data"]);
				
				for ($i = 0; $i < $j; $i++) {
				
					/* Variables */
					
					$refid = $apiResponse["response"]["data"]["data"][$i]["Stat"]["affiliate_info1"];
					$date = $apiResponse["response"]["data"]["data"][$i]["Stat"]["date"];
					$orderid = $apiResponse["response"]["data"]["data"][$i]["Stat"]["ad_id"];
					
				
					if (strstr($apiResponse["response"]["data"]["data"][$i]["Offer"]["name"], 'Credicasa')) {
						$campaignid = '72054a1c';
						echo call_postback() . "\n";
					} elseif (strstr($apiResponse["response"]["data"]["data"][$i]["Offer"]["name"], 'Nextel')) {
						$campaignid = 'db65c6c7';		
						echo call_postback() . "\n";									
					}
					
				}	
    }
    else {
        // An error occurred
        echo 'API call failed (' . $apiResponse['response']['errorMessage'] . ')';
        echo PHP_EOL;
        echo 'Errors: ' . print_r($apiResponse['response']['errors'], true);
        echo PHP_EOL;
    }

